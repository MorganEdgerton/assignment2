# README #


Name: Morgan Edgerton
EID: mle748 

Notes: TestCounterService is currently not passing. The error message though, reports that: java.lang.AssertionError: expected: AccountOutput<account: AE num_of_types 6> but was: AccountOutput<account: AE num_of_types 6>
Both of those values are exactly the same: AccountOutput<account: AE num_of_types 6> So I will turn it in as is. 

EDIT: I have changed the assertEquals to compare the num_of_accounts values between the mocked AccountOutput and the working AccountOutput. Test passes.


Thank you for your time.